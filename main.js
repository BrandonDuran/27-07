var Familia=[];

var persona1=[];
persona1.push("Brandon");
persona1.push("Duran");
persona1.push("24");
persona1.push("Estatura: 1.80");
persona1.push("Sangre: O+");


var persona2=[];
persona2.push("Teresa");
persona2.push("Rocha");
persona2.push("47");
persona2.push("Ojos: Negros");
persona2.push("Pelo: Negro");


var gustosBrandon=[];
gustosBrandon.push("Rock");
gustosBrandon.push("Rap");
gustosBrandon.push("Gears of war");
gustosBrandon.push("Mass Effect");
gustosBrandon.push("Lasagne");

var gustosTeresa=[]
gustosTeresa.push("Cocinar");
gustosTeresa.push("Regional mexicano");
gustosTeresa.push("Piques");
gustosTeresa.push("Enchiladas");
gustosTeresa.push("Ropa");

persona1.push(gustosBrandon);
persona2.push(gustosTeresa);
Familia.push(persona1);
Familia.push(persona2);

//Forma 2
var familia={
    "Persona 1": {
        "Nombre":"Brandon",
        "Apellido":"Duran",
        "Edad":"24",
        "Estatura":"1.80",
        "Sangre":"O+",
        "gustos":["Rock","Rap","Gears of war","Mass effect","Lasagna"]
    },
    "Persona 2":{
        "Nombre":"Teresa",
        "Apellido":"Rocha",
        "Edad":"47",
        "Ojos":"Negros",
        "Pelo":"Negro",
        "gustos":["Cocinar","Regional mexicano","Piques","Enchiladas","Ropa"]
    },
    "Persona 3":{
        "Nombre":"Jorge",
        "Apellido":"Solano",
        "Edad":"37",
        "Tez":"Moreno",
        "Profesion":"Ingeniero civil",
        "gustos":["Halo","Fifa","Futbol","Pozole","Carne Asada"]
    },
    "Persona 4":{
        "Nombre":"Kevin",
        "Apellido":"Aldana",
        "Edad":"17",
        "Estudios":"Preparatoria",
        "Mes de nacimiento":"Octubre",
        "gustos":["Carros","Trap","Basquetbol","Rap","Pizza"]
    },
    "Persona 5":{
        "Nombre":"Jesus",
        "Apellido":"Duran",
        "Edad":"30",
        "Apodo":"Goyo",
        "Año de nacimiento":"1992",
        "gustos":["Lakers","NWA","Addidas","Gorras","Tacos"]
    }
}

console.log(Familia);
console.log(familia);